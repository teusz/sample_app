module SessionsHelper

  def log_in(user) #logs the user in
    session[:user_id] = user.id
  end

  def current_user #returns current user if there is any
    if session[:user_id]
      @current_user ||= User.find_by(id: session[:user_id])
    end
  end

  def logged_in? #checks if some user is logged
    !current_user.nil?
  end
end
